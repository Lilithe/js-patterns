document.getElementById("hideSection").style.display = "none";
function constructHtml() {
    document.getElementById("hideSection").style.display = "flex"; 
    let h2 = document.createElement("h2");
    h2.className = "app-section__join-us--title";
    let textForH2 = document.createTextNode("Join Our Program");
    h2.appendChild(textForH2);
    let p = document.createElement("p");
    p.className = "app-section__join-us--subtitle";
    let textForP = document.createTextNode("Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
    p.appendChild(textForP);
    let div = document.createElement("div");
    div.className = "app-section__join-us__subscribe";
    let emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.id = 'email-data';
    emailInput.value = '';
    emailInput.className = 'app-section__join-us--email';
    emailInput.placeholder = 'Email';
    let submitBtn = document.createElement('input');
    submitBtn.id = 'submitbtn';
    submitBtn.type = 'submit';
    submitBtn.value = 'SUBSCRIBE';
    submitBtn.classList.add('app-section__button', 'app-section__join-us-btn');
    div.appendChild(emailInput);
    div.appendChild(submitBtn);
    document.getElementsByClassName("app-section__join-us")[0].appendChild(h2);
    document.getElementsByClassName("app-section__join-us")[0].appendChild(p);
    document.getElementsByClassName("app-section__join-us")[0].appendChild(div);
}
window.addEventListener('DOMContentLoaded', function() {
    constructHtml()
    document.getElementById("submitbtn").addEventListener("click", function(event) {
        event.preventDefault();
        console.log(document.getElementById("email-data").value)
    })
  });

class Program {
    constructor(submitBtn, textForH2) {
        this.submitBtn.value = submitBtn.value;
        this.textForH2 = textForH2;
    }
}

class SectionCreator {
    create(type) {
        switch(type) {
            case 'standart' :
            return new Program('SUBSCRIBE', "Join Our Program")

        }
        switch(type) {
            case 'advanced' :
            return new Program('Subscribe to Advanced Program', "Join Our Advanced Program")
        }
    }
}
var add = new Program(submitBtn, textForH2);
console.log(add)

export default constructHtml();
